<div class="modal fade"
     id="modalComment"
     tabindex="-1" role="dialog" aria-labelledby="ModalCommentList" aria-hidden="true">
    <div class="modal-dialog modal-lg custom-comments-modal" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close modalCLose" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h1 class="modal-title">Comments</h1>
                <!--<p>Search comments to reply</p>-->
            </div>
            <div class="modal-body">
                <form class="form hide" id="formComment">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="videoInfo"></div>
                            <br>
                            <div class="input-group">
                                <input id="searchQueryComment" name="searchQueryComment"
                                       class="form-control input-lg" placeholder="Enter Keyword"
                                       aria-label="Enter Keyword" type="search">
                                <span class="input-group-btn">
									<button id="searchCommentyt" name="searchCommentyt" type="submit"
                                            class="btn btn-default btn-lg">Search Comments</button>
                                </span>
                            </div><!-- /input-group -->
                        </div>
                    </div>
                </form>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div id="contentComment"></div>
                        <span class="hide" id="nextPageCommentToken"> </span>
                        <span class="hide" id="videoIdComment"> </span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" id="loadComments">Load more</button>
            </div>
        </div>
    </div>
</div>
