#!/bin/bash
#Get servers list
set -f
string=$AWS_DEPLOY_SERVER
array=(${string//,/ })
#Iterate servers for deploy and pull last commit
for i in "${!array[@]}"
do
      echo "Deploy project on aws lightsail server ${array[i]}"
      ssh ubuntu@${array[i]} "cd /var/www/html/demo-projects/youtube-api/ && sudo git pull origin master && sudo npm install"
done
