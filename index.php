<?php
require_once "config/config.php";
require_once "lib/helpers.php";

$visitorLocation = checkVisitorLocation();

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Web app to search/stream youtube videos via api.">
    <meta name="author" content="Yogendra Mohara">

    <title>Youtube Streamer</title>
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="node_modules/@fortawesome/fontawesome-free/css/all.css"/>
    <link rel="stylesheet" href="assets/css/style.css?<?= time() ?>"/>

</head>

<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-green layout-top-nav">

<div class="wrapper">

    <!-- Full Width Column -->
    <div class="content-wrapper">

        <div class="container">

            <form id="searchForm"
                action="">

                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-center logo"><img height="80px"
                                                             src="//suite.social/apps/images/youtube-logo.png"></p>
                            <p class="text-center">Enter keyword to search videos and show view count, comments, likes,
                                dislikes and reply to comments.</p>
                        </div>
                    </div>

                    <div class="row">

                            <div class="col-sm-12 col-md-2">
                                <select id="searchType" class="form-control input-lg">
                                    <option value="video">Search Videos</option>
                                    <option value="playlist">Search Playlists</option>
                                </select>
                            </div>

                            <div class="col-sm-12 col-md-8">
                                <input id="searchQuery" name="searchQuery" class="form-control input-lg"
                                       placeholder="Enter Keyword" aria-label="Enter Keyword" type="search">
                            </div>


                            <div class="col-sm-12 col-md-2"><button id="searchyt" name="searchyt" type="submit"
                                                                        class="btn btn-primary btn-block">Search!</button>
                            </div>

                    </div>

                    <div class="row hide" id="searchResponse"></div>

                </form>

                <div class="row">
                    <div class="col-md-12">
                        <div id="searchResultFor"></div>
                    </div>
                    <div class="col-md-12">
                        <div class="row bg-white rounded hide" id="contentyt"></div>
                    </div>
                </div>

                <div id="load_more_container" class="row hide">
                    <div class="col-md-12">
                        <button id="loadMoreyt" name="loadMoreyt" class="pull-right btn btn-danger btn-lg hide "
                                type="button">Load more
                        </button>
                        <span class="hide" id="nextPageToken"> </span>
                        <span class="hide" id="queryyt"> </span>
                    </div>
                </div>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.content-wrapper -->

    <?php require_once "partials/modal-video-comments.php"; ?>

    <!-- jQuery 3 -->
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <!-- popperjs
    <script src="node_modules/popper.js/umd/popper.min.js"></script>-->
    <!-- bootstrap 4  -->
    <script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- fontawesome  -->
    <script src="node_modules/@fortawesome/fontawesome-free/js/all.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>

    <?php
    /*
    * if the ip geo location api successfully returns visitor's location,
    * use this data to load popular videos according to the location in json response
    */
    if (is_array($visitorLocation) && key_exists('country_code2', $visitorLocation)) {
        echo "
        <script>
            let checkedSearchRegion = '" . $decodedLocation['country_code2'] . "';
            let checkedSearchRegionName = '" . $decodedLocation['country_name'] . "';
            console.log('visitor ip location check complete');
        </script>";
    } else echo "<script>console.log('failed to get visitor location');</script>";
    ?>

    <script src="assets/js/youtube.js?<?= time() ?>" type="text/javascript"></script>

</body>

</html>
