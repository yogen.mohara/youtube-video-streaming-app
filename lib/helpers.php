<?php

/**
 * @todo checks visitor ip address and determines the location
 * @return bool|array
 */
function checkVisitorLocation() {

    // if location info already present in cookie no need to check
    if(
        isset( $_COOKIE['visitor_country_code2'] )
        && isset( $_COOKIE['visitor_country_name'] )
    ) {
        return array(
            'country_code2' => $_COOKIE['visitor_country_code2'],
            'country_name' => $_COOKIE['visitor_country_name']
        );
    }

    $ip = getUserIpAddr();
    if( ! $ip ) return false;

    $location = get_geolocation($ip);
    if( ! $location ) return false;

    $ar_location = json_decode($location, true);
    if( ! is_array( $ar_location ) ) return false;

    //save location data to cookie
    setcookie( 'visitor_country_code2', $ar_location['countryCode'], time() + ( 60 * 60 * 24 ) , '/' ) ;
    setcookie( 'visitor_country_name', $ar_location['country'], time() + ( 60 * 60 * 24 ) , '/' ) ;

    return $ar_location;
}

/**
 * @todo checks the location of the supplied ip address
 * @param string $ip
 * @return bool|string
 */
function get_geolocation($ip) {

    global $ipCheckApiUrl;

    $url = $ipCheckApiUrl . $ip;

    $cURL = curl_init();

    curl_setopt($cURL, CURLOPT_URL, $url);
    curl_setopt($cURL, CURLOPT_HTTPGET, true);
    curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($cURL, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Accept: application/json'
    ));

    return curl_exec($cURL);
}

/**
 * @todo determines the ip address of the visitor
 * @return string
 */
function getUserIpAddr(){

    $ip = $_SERVER['REMOTE_ADDR'];

    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
        //ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }
    else if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        //ip pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }

    return $ip;
}

/* for development only */
function dump( $dump ) {
    if( isset( $_COOKIE['debug_mode']) ) {
        var_dump( $dump );
    }
    return;
}

/* for development only */
function pre( $dump ) {
    if( isset( $_COOKIE['debug_mode']) && is_array( $dump ) ) {
        echo "<pre>";
        print_r( $dump );
        echo "</pre>";
    }
    return;
}
