<?php

// enable debug cookie
if( isset( $_GET['set_cookie_debug'] ) && $_GET['set_cookie_debug'] = '1' ) {
	setcookie( 'debug_mode', '1', time() + ( 60 * 60 * 24 ) , '/' ) ;
}

// disable debug cookie
if( isset( $_GET['set_cookie_debug'] ) && $_GET['set_cookie_debug'] = '0' ) {
	setcookie( 'debug_mode', '0', time() - 3600) ;
}

// enable error reporting for dev mode
if(isset($_COOKIE['set_cookie_debug'])) ini_set("display_errors", 1 );

// ip geo location api url
$ipCheckApiUrl = "http://extreme-ip-lookup.com/json/";
