<p>This project is a web app that allows to search/watch Youtube's videos & playlists via Youtube's API.</p>
<p>The video listing varies according to the location of the visitor.</p>
<p>You can check out the demo <a raget="_blank" href="https://demo.yogenm.uk/youtube-api/">here</a>.</p>