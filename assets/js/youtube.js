/**
 * Author Christelle skype: @chris_kelly2
 * Author : Yogendra Mohara
 */

// youtube api details
const API_URL = 'https://www.googleapis.com/youtube/v3/';
const API_KEY = 'AIzaSyBmo5hFKf9uDS7xhYbVyrWhoS4t-dmb0vA'; // this should be stored somewhere else secure

let searchRegion = 'GB';                    // degault region code for loading trending videos
let searchRegionName = 'United Kingdom';    // default region name for loading trending videos
let searchCategory = 0;                     // default category id for loading trending videos | 0 => all categories

// if visitor's ip geo location check is successful, then use them in the youtube api paramters
if( typeof checkedSearchRegion !== 'undefined' && checkedSearchRegion !== "" ) {
    searchRegion = checkedSearchRegion;
}
if( typeof checkedSearchRegionName !== 'undefined' && checkedSearchRegionName !== "" ) {
    searchRegionName = checkedSearchRegionName;
}
// end

let itemsPerPage = 12;                                      // number of search results per page
let searchType = 'video';                                   // can be either video or playlist or channel
let searchQuery;                                            // stores the search keyword entered by user
let fetchMode = 'popular';                                  // can be either 'popular' or 'search'
let videoEmbedModalId = "videoModal";                       // unique id of the modal element for embedding videos/playlists
let viewsCountFormatter = new Intl.NumberFormat('en-US');   // for number formatting the view count values

/* dimensions of the current page wrapper */
let wrapperHeight = $('.custom-video-modal').height();
let wrapperWidth = $('.custom-video-modal').width();

/*set modal dimension according to wrapper dimensions*/
let modalHeight = wrapperHeight * 0.75;
let modalWidth = wrapperWidth * 0.8;

/* function to prep video modal */
function videoModal( videoId, videoTitle ) {
    let embedUrl = '<iframe class="custom-video-iframe" src="https://www.youtube.com/embed/'+videoId+'?&autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
    prepModal( videoTitle, embedUrl);
    return;
}

/* function to prep playlist modal */
function playlistModal( playlistId, playlistTitle) {
    let embedUrl = '<iframe class="custom-video-iframe" src="https://www.youtube.com/embed/videoseries?list='+playlistId+'&autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
    prepModal( playlistTitle, embedUrl);
    return;
}

/**
 * Function to build & trigger modal box to embed youtube video/playlist.
 * @param {video/playlist title to be displayed as modal title } modalTitle
 * @param {url of the video/playlist to be embedded } embebYoutubeUrl
 */
function prepModal( modalTitle, modalBody) {

    $("#"+videoEmbedModalId).remove();

    // prep modal element
    let modalBox = '<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModalTitle" aria-hidden="true">'+
                        '<div class="modal-dialog modal-dialog-centered custom-video-modal" role="document">'+
                            '<div class="modal-content">'+
                                '<div class="modal-header">'+
                                    '<h5 class="modal-title" id="videoModalTitle">'+ modalTitle +'</h5>'+
                                    '<button type="button" class="close modalCLose" data-dismiss="modal" aria-label="Close">'+
                                        '<span aria-hidden="true">&times;</span>'+
                                    '</button>'+
                                '</div>'+
                                '<div class="modal-body">'+
                                    modalBody+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>';

    // add modal to DOM
    $(".wrapper").append(modalBox);

    // fire modal
    $("#"+videoEmbedModalId).modal({
        'backdrop': true,
        'keyboard': true
    });

    return;
}

/* display error message as supplied in the paramter */
function doErrorMessage( msg ) {
    $("#searchResponse").removeClass("hide").html(getErrorBox( msg ));
    return;
}

/* return the error box html */
function getErrorBox( msg ) {
    return '<div class="col-sm-12"><div class="alert alert-danger mt-2">' + msg + '</div></div>'
}

/**
 * Function to retrieve all the stat of the videos
 * @param {} VideoId
 */
async function getVideoStat( VideoId, title) {

    let urlAPI = API_URL + 'videos' +
        '?part=statistics' +
        '&id=' + VideoId +
        '&key=' + API_KEY;

    let divHtml = "";
    await Promise.resolve($.ajax({
        url: urlAPI,
        type: 'GET',
        success: function(res) {

            if(res.items.length > 0)
            {
                res.items.forEach( element =>{

                    let viewCount =  element.statistics.hasOwnProperty("viewCount") ?  element.statistics.viewCount  : 0;
                    let commentCount =  element.statistics.hasOwnProperty("commentCount") ?  element.statistics.commentCount  : 0;
                    let likeCount =  element.statistics.hasOwnProperty("likeCount") ?  element.statistics.likeCount  : 0;
                    let dislikeCount =  element.statistics.hasOwnProperty("dislikeCount") ?  element.statistics.dislikeCount  : 0;
                    let favCount =  element.statistics.hasOwnProperty("favoriteCount") ?  element.statistics.favoriteCount  : 0;

                    viewCount = formatViewCount(viewCount);
                    commentCount = kFormatter(commentCount);
                    likeCount = kFormatter(likeCount);
                    dislikeCount = kFormatter(dislikeCount);
                    favCount = kFormatter(favCount);

                    // if the comment stat is zero then display a text otherwise display a link
                    let hasComments = '<span title="' + commentCount + ' Comments"><a href="#" class="ml-2 linkComment " id="' + element.id + '"><i class="fas fa-comment"></i> (' + commentCount + ')</a></span> - ';
                    let notHasComments = '<span title="'+ commentCount + ' Comments"> <i class="fas fa-comment"></i> (0)</span> - ';
                    let htmlForComment = commentCount == 0 ? notHasComments : hasComments;

                    divHtml += "<p class='videoStats'>";
                    divHtml += "<span> <span class='viewCountBox'>"+  viewCount + "</span> Views - </span>";
                    divHtml +=  htmlForComment;
                    divHtml += "<span title=\""+  likeCount + " Likes\"> <i class='fas fa-thumbs-up'></i> ("+  likeCount + ") - </span>";
                    divHtml += "<span title=\""+  dislikeCount + " Dislikes\"> <i class='fas fa-thumbs-down'></i> ("+  dislikeCount + ") - </span>";
                    divHtml += "<span title=\""+  favCount + " Favorites\"> <i class='fas fa-heart'></i> ("+  favCount + ")</span>";
                    divHtml += "</p>";

                    // set the videos as a session storage
                    let keyStorage = String(element.id) + "";
                    sessionStorage.setItem(keyStorage,  JSON.stringify({"title": title, "View": viewCount, "Comment": commentCount }));
                });
            }
        }
    }));
    return  divHtml;
}

/* format the supplied number with thousand separators */
function formatViewCount(count) {
    return viewsCountFormatter.format( count );
}

/* format the supplied number in the K format*/
function kFormatter(num) {
    return Math.abs(num) > 999 ? Math.sign(num)*((Math.abs(num)/1000).toFixed(1)) + 'k' : Math.sign(num)*Math.abs(num)
}

/**
 * Function to retrieve all the stat of the playlist
 * @param {} playListId
 * @param {} title
 */
async function getPlaylistStat( playListId, title) {

    let urlAPI = API_URL + 'playlists' +
        '?part=contentDetails' +
        '&id=' + playListId +
        '&key=' + API_KEY;

    var divHtml = "";
    await Promise.resolve($.ajax({
        url: urlAPI,
        type: 'GET',
        success: function(res) {

            if(res.items.length > 0)
            {
                res.items.forEach( element =>{
                    let videoCount =  element.contentDetails.hasOwnProperty("itemCount") ?  element.contentDetails.itemCount  : 0;

                    videoCount = formatViewCount(videoCount);
                    divHtml += "<p class='videoStats'>";
                    divHtml += "<a class='ytColor' title='Click here to watch this playlist' onclick=\"return playlistModal( '" + playListId + "', '" + title + "');\"><span class = \"ml-2  \"> Videos ("+  videoCount + ")</span></a>";
                    divHtml += "</p>";

                    // set the videos as a session storage
                    let keyStorage = String(element.id) + "";
                    sessionStorage.setItem(keyStorage,  JSON.stringify({"title": title, "Videos": videoCount }));
                });
            }
        }
    }));
    return  divHtml;
}

/*
* function to fetch search results from youtube api via ajax
* @param { url for youtube api } youtubeUrlAPi
* */
function loadSearchResults(youtubeUrlAPi) {

    let srcTyp = searchType;
    $("#searchResponse").html('').addClass("hide");

    $.ajax({
        url: youtubeUrlAPi,
        type: 'GET',
        success: function(res) {

            $('#contentyt').append("<div class='col-sm-12'><div class='row' id='listingsHolder'></div></div>");

            // process each result set
            res.items.forEach(element => {

                // if search type is video
                if( srcTyp == 'video' ) {

                    // fetch mode 'popular' returns json in a different format than that of 'search' mode, hence this check
                    let videoId = element.id;
                    if( fetchMode == 'search' ) {
                        videoId = element.id.videoId;
                    }

                    // retrieve the videos statitics, then render html
                    getVideoStat(videoId, element.snippet.title ).then( resultStat => {
                        buildVideoList(element, resultStat);
                    });
                }
                // if search type is playlist
                else if( srcTyp == 'playlist' ) {
                    // retrieve the playlist statitics, then render html
                    getPlaylistStat(element.id.playlistId, element.snippet.title ).then( resultStat => {
                        buildPlayList(element, resultStat);
                    });
                }
                else {
                    doErrorMessage('Invalid Search Type');
                    return false;
                }
            });

            // set the next page token on the page
            $('#nextPageToken').html(res.nextPageToken);
            // display the load more button
            $('#loadMoreyt').removeClass('hide')
            //display the div since it was hidden
            $('#contentyt').removeClass('hide');
            // show load more button
            $('#load_more_container').removeClass('hide');
        },
        error: function(res) {
            if( res.responseJSON.hasOwnProperty('error')) {
                doErrorMessage(res.responseJSON.error.message)
                return false;
            }
        }
    });
}

/*
* this function builds html for video list from json
 * @param {video element } element
 * @param {video element statistics } elementStat
* */
function buildVideoList(element, elementStat) {
    let div = "<div class=\"col-sm-12 col-md-4\"><div class=\"listItemBox\">";

    // fetch mode 'popular' returns json in a different format than that of 'search' mode, hence this check
    let videoId = element.id;
    if( fetchMode == 'search' ) {
        videoId = element.id.videoId;
    }

    div += "<div class='videoThumbnailBox'><a onclick=\"return videoModal( '" + videoId + "', '" + element.snippet.title + "');\" href=\"#!\"><img class = \"img-fluid\" src=\"" + element.snippet.thumbnails.high.url + "\"" + "width=\"100%\" height=\"100%\" alt=\"...\"/></a></div>";
    div += "<div class='videoInfoBox'><p class='videoTitle' title='"+element.snippet.title+"'><a class=\"ytColor\" onclick=\"return videoModal( '" + videoId + "', '" + element.snippet.title + "');\" href=\"#!\" rel=\"nofollow\">";
    div += ( element.snippet.title != "" ?  element.snippet.title.length > 40 ? element.snippet.title.substring(0, 39) + " ...." : element.snippet.title : "Description not available" ) + "</a></p>";
    div += "<p class='videoDescription' title='"+element.snippet.description+"'> " + ( element.snippet.description != "" ? element.snippet.description.length > 70 ? element.snippet.description.substring(0, 69) + " ...." : element.snippet.description : "Description not available" ) +"</p>"
    // add stat here
    div += elementStat
    div += "</div></div></div>";

    // append videoas details to div in index.html
    $('#listingsHolder').append(div);
}

/*
* this function builds html for playlist from json
 * @param {playlist element } element
 * @param {playlist element statistics } elementStat
* */
function buildPlayList(element, elementStat) {


    let div = "<div class=\"col-sm-12 col-md-4\"><div class=\"listItemBox\">";
    div += "<div class='videoThumbnailBox'><a onclick=\"return playlistModal( '" + element.id.playlistId + "', '" + element.snippet.title + "');\" href=\"#!\"><img class = \"img-fluid\" src=\"" + element.snippet.thumbnails.high.url + "\"" + "width=\"100%\" height=\"100%\" alt=\"...\"/></a></div>";
    div += "<div class='videoInfoBox'><p class='videoTitle' title='"+element.snippet.title+"'><a class=\"ytColor\" onclick=\"return playlistModal( '" + element.id.playlistId + "', '" + element.snippet.title + "');\" href=\"#!\" rel=\"nofollow\">";
    div += ( element.snippet.title != "" ?  element.snippet.title.length > 50 ? element.snippet.title.substring(0, 49) + " ...." : element.snippet.title : "Description not available" ) + "</a></p>";
    div += "<p class='videoDescription' title='"+element.snippet.description+"'> " + ( element.snippet.description != "" ? element.snippet.description.length > 100 ? element.snippet.description.substring(0, 99) + " ...." : element.snippet.description : "Description not available" ) +"</p>"
    // add stat here
    div += elementStat
    div += "</div></div></div>";

    // append videoas details to div in index.html
    $('#listingsHolder').append(div);
}

/**
 * Function to handle   ajax call to get the comment of a video.
 * @param {url of the comment } youtubeUrlAPi
 */
function loadCommentOfVideo(youtubeCommentUrlAPi){

    // ajax call to get the comments
    $.ajax({
        url: youtubeCommentUrlAPi,
        type: 'GET',
        success: function(res) {
            var  div ="";
            //when there are fewer comment  than 20 hide the load more button in modal
            if ( res.pageInfo.totalResults < 20 ) {
                $('#loadComments').addClass("hide");
            }
            // otherwise display the button
            else {
                $('#loadComments').removeClass("hide");
            }

            res.items.forEach( element=>{

                // when there are comment display serach comment form
                $('#formComment').removeClass("hide");

                if (res.pageInfo.totalResults > 0)
                {
                    if(element.snippet.isPublic)
                    {
                        div += "<div class=\"row mb-1 w-100 commentContainer\">";
                        div += "<div class=\"col-2 col-md-1\">";
                        div += "<img class=\"rounded-circle\" src=\"" + element.snippet.topLevelComment.snippet.authorProfileImageUrl + "\"" + "width=\"50px\" height=\"50px\" alt=\"...\"/>";
                        div += "</div>";
                        div += "<div class=\"col-8 col-md-11\">";
                        //***** */
                        div += "<div class=\"row\">";
                        div += "<div class=\"col\">";
                        div += "<h4 class=\"h4 mt-0\">";
                        div += "<a class=\"ytColor\" href=\""+element.snippet.topLevelComment.snippet.authorChannelUrl + "\" target=\"_blank\" rel=\"noopener noreferrer\"> ";
                        div += element.snippet.topLevelComment.snippet.authorDisplayName + "</a>";
                        div += "</h4>";
                        div += "</div>";
                        div += "</div>";

                        div += "<div class=\"row\">";
                        div += "<div class=\"col\">";
                        div += "<p> " + element.snippet.topLevelComment.snippet.textOriginal +"</p>"
                        div += "</div>";
                        div += "</div>";

                        div += "<div class=\"row\">";
                        div += "<div class=\"col\">";
                        div += "<p>  <a class =\"mr-3 ytColor linkShowComments\" href=\"https://www.youtube.com/watch?v=" + element.snippet.topLevelComment.snippet.videoId + "&lc=" + element.id +  "\" target=\"_blank\"   rel=\"nofollow\"> Show comments </a>"
                        div += "<i class='fas fa-thumbs-up'></i> (" + element.snippet.topLevelComment.snippet.likeCount +") "
                        div += "<span class=\"ml-3\ \" id=\"\"> <i class='fas fa-comment'></i> (" +  element.snippet.totalReplyCount + ") </span> "
                        let dateCreation = new Date(element.snippet.topLevelComment.snippet.updatedAt);
                        div += "<span class=\"ml-3 commentDate\">" +  dateCreation.toDateString() + " </span> </p>"
                        div += "</div>";
                        div += "</div>";
                    }
                    if(element.hasOwnProperty('replies'))
                    {
                        div += "<div class=\"ytReplies hide\">"
                        element.replies.comments.forEach( item =>{
                            div += "<div class=\"row w-100  \" id=\"showReply\">";
                            div += "<div class=\"col-2 col-md-1\">";
                            div += "<img class=\"rounded-circle\" src=\"" + item.snippet.authorProfileImageUrl + "\"" + "width=\"50px\" height=\"50px\" alt=\"...\"/>";
                            div += "</div>";
                            div += "<div class=\"col-8 col-md-11\">";
                            //***** */
                            div += "<div class=\"row\">";
                            div += "<div class=\"col\">";
                            div += "<h4 class=\"h4\">";
                            div += "<a class=\" \" href=\"https://www.youtube.com/watch?v="+ element.id.videoId + ";google_comment_id="+item.id+" target=\"_blank\" rel=\"noopener noreferrer\">";
                            div += item.snippet.authorDisplayName + "</a>";
                            div += "</h4>";
                            div += "</div>";
                            div += "</div>";

                            div += "<div class=\"row\">";
                            div += "<div class=\"col\">";
                            div += "<p> " + item.snippet.textOriginal +"</p>"
                            div += "</div>";
                            div += "</div>";

                            div += "<div class=\"row\">";
                            div += "<div class=\"col\">";
                            div += "<p class=\" \">"
                            div += "Likes " + item.snippet.likeCount +" ";
                            var dateCreationR = new Date(item.snippet.updatedAt);
                            div += "<span class=\"ml-5\">" +  dateCreationR.toDateString() + " </span> ";
                            div += "</p>"
                            div += "</div>";
                            div += "</div>";
                            div += "</div>";
                            div += "</div>";
                        });

                        div += "</div>"
                    }

                    div += "</div>";
                    div += "</div>";
                    div += "<hr>";
                }
                else{
                    return;
                }
            });

            // set the next page token on the page
            $('#nextPageCommentToken').html(res.nextPageToken);
            $('#contentComment').append(div);
            $('#modalComment').modal('show')
        }
    });
}

$(document).ready(function () {

    console.log('visitor country code: ' + searchRegion);
    console.log('visitor country name: ' + searchRegionName);
    console.log('init popular videos');

    /* show trending videos according to visitor's ip location */
    loadPopularVideos( searchRegion, searchRegionName, searchCategory );

    /**
     * Event to handle search form submit
     * make an ajax call to retrieve youtube videos
     */
    $(document).on('submit', '#searchForm', function(e){

        $("#searchResultFor").text('');

        e.preventDefault();

        fetchMode = 'search';
        // search keyword
        searchQuery = $('#searchQuery').val();
        // search type: video or playlist or channel
        searchType = $('#searchType').val();
        //save the query in a span for the next page
        $('#queryyt').text(searchQuery);

        let urlApi = API_URL + 'search' +
            '?part=snippet' +
            '&order=date' +
            '&relevanceLanguage=en'+
            '&q=' + searchQuery +
            '&type=' + searchType +
            '&key=' + API_KEY +
            '&maxResults=' + itemsPerPage;

        // clear the previous content if there is any
        $('#contentyt').empty();

        // load the  yt videos
        loadSearchResults(urlApi);
    });

    /* pagination for search results */
    $(document).on('click', '#loadMoreyt', function(){

        fetchMode = 'search';
        // get the next page token
        let nextPageToken = $('#nextPageToken').html();
        // get the query term
        let queryToSearch = $('#queryyt').html();
        // set the page token and the query
        let urlApi = API_URL + 'search' +
            '?part=snippet' +
            '&order=date' +
            '&pageToken=' + nextPageToken +
            '&q=' + searchQuery  +
            '&type=' + searchType +
            '&key=' + API_KEY +
            '&maxResults=' + itemsPerPage;
        // load the  yt videos  by appending them to the page
        loadSearchResults(urlApi);
    })

    /**
     * Event to handle the comment link clicked
     */
    $(document).on('click', 'a.linkComment', function(e){

        // get the id  of the videos set as the is of a href element/tag
        let videoId = $(this).attr('id');

        // set the video id for the load more comment  in the modal
        $('#videoIdComment').html(videoId);

        //create the  yt api url
        let apiUrl = API_URL + 'commentThreads' +
            '?part=replies%2C%20snippet' +
            '&order=time' +
            '&videoId='+ videoId +
            '&key='+ API_KEY ;

        let videoInfo = sessionStorage.getItem(videoId)

        // clear modal content
        $('#contentComment').empty();
        $('#videoInfo').empty();
        //load/ display comments
        loadCommentOfVideo(apiUrl);

         // Get  video infor from the session storage
         let videoObjectInfo =  JSON.parse(videoInfo);
         let htmlVideoInfor  = "<b><p class=\"h4 ytColor\"> Title: " + videoObjectInfo.title + "</p></b>";
         htmlVideoInfor  += "<span class=\"mr-3 h4 ytColor\"> " + videoObjectInfo.View + " Views</span>";
         htmlVideoInfor  += "<span class=\"mr-3 h4 ytColor\"> <i class='fas fa-comment'></i> (" + videoObjectInfo.Comment + ")</span>";
         $('#videoInfo').append("<p>"+ htmlVideoInfor + "</p>")
    });

    /**
     * Function to  load more comment
     */
    $(document).on('click', '#loadComments', function(){
        // get the id  of the videos set as the is of a href element/tag
        let videoId = $('#videoIdComment').html();

        let commentPagetToken = $('#nextPageCommentToken').html();
        //create the  yt api url
        let apiUrl = API_URL + 'commentThreads' +
            '?part=replies%2C%20snippet' +
            '&order=time' +
            '&pageToken='+commentPagetToken +
            '&videoId='+ videoId +
            '&key='+ API_KEY ;

        //load/ display comments
        loadCommentOfVideo(apiUrl);
    })

    /**
     * Function to handle replies click and display hidden images
     */
    $(document).on('click', 'span.ytColor', function(){
        $(this).closest('div.row').next().removeClass('hide');
    });

    /**
     * Function to handle commen serach button when clicked
     */
    $(document).on('submit', '#formComment', function(e){

        e.preventDefault();

        let commentToSearch =  $('#searchQueryComment').val();
        let videoId = $('#videoIdComment').html();
        let apiUrl = API_URL + 'commentThreads' +
            '?part=replies%2C%20snippet' +
            '&order=time' +
            '&searchTerms='+ commentToSearch +
            '&videoId='+ videoId +
            '&key='+ API_KEY ;

        // clear previous comment
        $('#contentComment').empty();

        //load/ display comments
        loadCommentOfVideo(apiUrl);
    });

    /*
    Function to destroy modal element when it is closed or hidden
    this is needed to stop videoplack when modal is closed
     */
    $(document).on('hidden.bs.modal','#'+videoEmbedModalId,function(e){
        $('#'+videoEmbedModalId).remove();
    });
});

function loadPopularVideos( listingRegion, listingRegionName, searchCategoryId ) {

    $("#searchResultFor").text("Popular videos in " + listingRegionName);
    let urlApi = API_URL + 'videos' +
        '?part=snippet' +
        '&chart=mostPopular' +
        '&regionCode=' + listingRegion +
        '&videoCategoryId=' + searchCategoryId +
        '&key=' + API_KEY +
        '&maxResults=' + itemsPerPage;

    // clear the previous content if there is any
    $('#contentyt').empty();

    // load the  yt videos
    loadSearchResults(urlApi);
}


